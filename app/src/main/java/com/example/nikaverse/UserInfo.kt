package com.example.nikaverse


data class UserInfo(
    val username: String = "",
    val email: String = "",
    val uid: String = "",
)

