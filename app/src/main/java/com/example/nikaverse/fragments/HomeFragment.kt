package com.example.nikaverse.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.nikaverse.ChatMessages
import com.example.nikaverse.R
import com.example.nikaverse.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item

class HomeFragment : Fragment(R.layout.fragment_home) {
    private lateinit var recyclerView: RecyclerView
    private lateinit var logoutImageView: ImageView
    private lateinit var createImageView: ImageView
    private val db = FirebaseDatabase.getInstance().getReference("/userinfo")
    private val adapter = GroupieAdapter()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val uid = FirebaseAuth.getInstance().currentUser?.uid // checking current user auth
        if (uid == null) {
            val action = HomeFragmentDirections.actionHomeFragmentToLoginFragment()
            findNavController().navigate(action)
        }
        init()
        recyclerView.adapter = adapter
        listenForLatestMessages()
        registerListeners()
    }

    private fun init() {
        recyclerView = view?.findViewById(R.id.recyclerViewLatestMessages)!!
        logoutImageView = view?.findViewById(R.id.imageView)!!
        createImageView = view?.findViewById(R.id.imageViewMessages)!!
    }

    private fun registerListeners() {
        logoutImageView.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val action = HomeFragmentDirections.actionHomeFragmentToLoginFragment()
            findNavController().navigate(action)
        }
        createImageView.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToCreateMessageFragment()
            findNavController().navigate(action)
        }
    }

    val messagesMap = HashMap<String, ChatMessages>()
    private fun refreshRecyclerView() {
        adapter.clear()
        messagesMap.values.forEach {
            adapter.add(LatestMessageRow(it))        }

    }

    private fun listenForLatestMessages() {
        db.child(FirebaseAuth.getInstance().uid.toString()) // current username rom vipovot logikistvis
            .addValueEventListener(object : ValueEventListener { // search for current username
                override fun onDataChange(snapshot: DataSnapshot) {
                    val userInfo: UserInfo = snapshot.getValue(UserInfo::class.java) ?: return
                    val fromUser = userInfo.username // current username
                    val ref =
                        FirebaseDatabase.getInstance().getReference("/latest-messages/$fromUser")
                    ref.addChildEventListener(object : ChildEventListener {
                        override fun onChildAdded(
                            snapshot: DataSnapshot,
                            previousChildName: String?
                        ) {
                            val latestMessage =
                                snapshot.getValue(ChatMessages::class.java) ?: return
                            messagesMap[snapshot.key!!] = latestMessage
                            refreshRecyclerView()
                            adapter.setOnItemClickListener { _, view ->
                                val username: TextView = view.findViewById(R.id.textViewUserNameLatest)
                                val argument = username.text.toString()
                                val action = HomeFragmentDirections.actionHomeFragmentToMessageFragment(argument)
                                findNavController().navigate(action)
                            }
                        }

                        override fun onChildChanged(
                            snapshot: DataSnapshot,
                            previousChildName: String?
                        ) {
                            val latestMessage =
                                snapshot.getValue(ChatMessages::class.java) ?: return
                            messagesMap[snapshot.key!!] = latestMessage
                            refreshRecyclerView()
                        }

                        override fun onChildRemoved(snapshot: DataSnapshot) {
                        }

                        override fun onChildMoved(
                            snapshot: DataSnapshot,
                            previousChildName: String?
                        ) {
                        }

                        override fun onCancelled(error: DatabaseError) {
                        }
                    })
                }

                override fun onCancelled(error: DatabaseError) {
                }

            })
    }

    class LatestMessageRow(val chatMessages: ChatMessages) : Item<GroupieViewHolder>() {
        override fun getLayout() = R.layout.latest_messages_row

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.findViewById<TextView>(R.id.textViewLatestMessage).text = chatMessages.text
            if(FirebaseAuth.getInstance().uid.toString() == chatMessages.fromUid){
                viewHolder.itemView.findViewById<TextView>(R.id.textViewUserNameLatest).text = chatMessages.toUser
            }
            else{
                viewHolder.itemView.findViewById<TextView>(R.id.textViewUserNameLatest).text = chatMessages.fromUser
            }

        }

    }
}