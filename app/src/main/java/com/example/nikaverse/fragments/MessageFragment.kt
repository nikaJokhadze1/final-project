package com.example.nikaverse.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.nikaverse.ChatMessages
import com.example.nikaverse.R
import com.example.nikaverse.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item

class MessageFragment : Fragment(R.layout.fragment_message) {
    private lateinit var recyclerView: RecyclerView
    private lateinit var textView: TextView
    private lateinit var buttonBack: ImageView
    private lateinit var editTextChat: EditText
    private lateinit var buttonSend: ImageView
    private val adapter = GroupieAdapter()
    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseDatabase.getInstance().getReference("/userinfo")
    private val dbMessages = FirebaseDatabase.getInstance()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_message, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)
        getInfoFromCreateMessage()
        registerListeners()
        buttonSend.setOnClickListener {
            sendMessage()
        }
        retrieveMessages()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    val action = MessageFragmentDirections.actionMessageFragmentToHomeFragment()
                    findNavController().navigate(action)
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    private fun retrieveMessages() {
        db.child(FirebaseAuth.getInstance().uid.toString())
            .addValueEventListener(object : ValueEventListener { // search for current username
                override fun onDataChange(snapshot: DataSnapshot) {
                    val userInfo: UserInfo = snapshot.getValue(UserInfo::class.java) ?: return
                    val fromUser = userInfo.username // current username
                    textView.text =
                        MessageFragmentArgs.fromBundle(requireArguments()).argument // receiver username
                    val toUser = textView.text
                    dbMessages.getReference("/user-messages/$fromUser/$toUser")
                        .addChildEventListener(object : ChildEventListener {
                            override fun onChildAdded(
                                snapshot: DataSnapshot,
                                previousChildName: String?
                            ) {
                                val chatMessage = snapshot.getValue(ChatMessages::class.java)
                                if (chatMessage != null) {
                                    if (chatMessage.toUser != toUser.toString())
                                        adapter.add(ChatItem(chatMessage.text))
                                    else {
                                        adapter.add(ChatItemTo(chatMessage.text))
                                    }
                                }
                                recyclerView.scrollToPosition(adapter.itemCount - 1)
                            }

                            override fun onChildChanged(
                                snapshot: DataSnapshot,
                                previousChildName: String?
                            ) {
                            }

                            override fun onChildRemoved(snapshot: DataSnapshot) {
                            }

                            override fun onChildMoved(
                                snapshot: DataSnapshot,
                                previousChildName: String?
                            ) {
                            }

                            override fun onCancelled(error: DatabaseError) {
                            }

                        })
                }

                override fun onCancelled(error: DatabaseError) {
                }
            }
            )
    }

    private fun init() {
        recyclerView = view?.findViewById(R.id.recyclerViewChatLog)!!
        textView = view?.findViewById(R.id.textViewNameChat)!!
        buttonBack = view?.findViewById(R.id.backButton)!!
        editTextChat = view?.findViewById(R.id.editTextMessage)!!
        buttonSend = view?.findViewById(R.id.buttonSend)!!
    }

    private fun getInfoFromCreateMessage() {
        textView.text = MessageFragmentArgs.fromBundle(requireArguments()).argument
        textView.setText(textView.text)

    }

    private fun sendMessage() {
        val text = if (editTextChat.text.toString().isNullOrBlank()) return Toast.makeText(
            activity,
            "Enter a message", Toast.LENGTH_SHORT
        ).show() else editTextChat.text.toString() // message that user enters
        textView.text = MessageFragmentArgs.fromBundle(requireArguments()).argument
        db.child(auth.currentUser?.uid!!).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val userInfo: UserInfo = snapshot.getValue(UserInfo::class.java) ?: return
                val fromUser = userInfo.username
                val toUser = textView.text
                val reference =
                    FirebaseDatabase.getInstance().getReference("/user-messages/$fromUser/$toUser")
                        .push()
                val referenceTo =
                    FirebaseDatabase.getInstance().getReference("/user-messages/$toUser/$fromUser")
                        .push()
                val chatMessage = ChatMessages(
                    reference.key!!,
                    text,
                    fromUser,
                    toUser.toString(),
                    System.currentTimeMillis() / 1000,
                    auth.currentUser?.uid.toString()
                )
                reference.setValue(chatMessage).addOnSuccessListener {
                    editTextChat.text.clear()
                    recyclerView.scrollToPosition(adapter.itemCount - 1)
                }
                if (fromUser != toUser) {
                    referenceTo.setValue(chatMessage) // rodesac shen tavs ugzavni rom ar gameordes mesijebi
                }
                val latestMessageRef = FirebaseDatabase.getInstance()
                    .getReference("/latest-messages/$fromUser/$toUser")
                latestMessageRef.setValue(chatMessage)
                val latestMessageRefTo = FirebaseDatabase.getInstance()
                    .getReference("/latest-messages/$toUser/$fromUser")
                latestMessageRefTo.setValue(chatMessage) // es uaxlesi mesijebistvis , rom gamochndes home fragmentze
            }

            override fun onCancelled(error: DatabaseError) {
            }
        }
        )
    }

    private fun registerListeners() {
        buttonBack.setOnClickListener {
            val action = MessageFragmentDirections.actionMessageFragmentToHomeFragment()
            findNavController().navigate(action)
        }
    }


}


class ChatItem(val text: String) : Item<GroupieViewHolder>() {
    override fun getLayout() = R.layout.chat_log_row

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.findViewById<TextView>(R.id.textViewMessageFrom).text = text
    }
}

class ChatItemTo(val text: String) : Item<GroupieViewHolder>() {
    override fun getLayout() = R.layout.chat_log_row_sender

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.findViewById<TextView>(R.id.textViewMessageTo).text = text
    }
}
