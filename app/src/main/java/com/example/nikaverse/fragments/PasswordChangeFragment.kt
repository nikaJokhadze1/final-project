package com.example.nikaverse.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.nikaverse.R
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import java.util.regex.Pattern

class PasswordChangeFragment : Fragment(R.layout.fragment_password_change) {
    private lateinit var layoutPass: TextInputLayout
    private lateinit var editTextPassword: TextInputEditText
    private lateinit var buttonChangePassword: Button
    private lateinit var buttonBack: ImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_password_change, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        registerListeners()
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true)
            {
                override fun handleOnBackPressed() {
                    val action = PasswordChangeFragmentDirections.actionPasswordChangeFragmentToProfileFragment()
                    findNavController().navigate(action)
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }
    private fun init() {
        layoutPass = view?.findViewById(R.id.layoutPassword)!!
        editTextPassword = view?.findViewById(R.id.editTextPassword)!!
        buttonChangePassword = view?.findViewById(R.id.buttonChangePassword)!!
        buttonBack = view?.findViewById(R.id.backButton)!!
    }

    private fun registerListeners() {
        buttonChangePassword.setOnClickListener {
            val newPassword = editTextPassword.text.toString()
            if (newPassword.isEmpty() || newPassword.length < 8) {
                Toast.makeText(
                    activity,
                    "Password must be at least 8 characters long",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (!isValidPassword(newPassword)) {
                Toast.makeText(
                    activity,
                    "Password must contain Letters and Numbers",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)
                ?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(activity, "Password updated", Toast.LENGTH_SHORT).show()
                        val action =
                            PasswordChangeFragmentDirections.actionPasswordChangeFragmentToProfileFragment()
                        findNavController().navigate(action)
                    } else {
                        Toast.makeText(activity, "Errotkh!", Toast.LENGTH_SHORT).show()
                    }
                }
        }
        buttonBack.setOnClickListener {
            val action =
                PasswordChangeFragmentDirections.actionPasswordChangeFragmentToProfileFragment()
            findNavController().navigate(action)
        }
    }
    private fun isValidPassword(password: String): Boolean {
        val passwordREGEX = Pattern.compile(
            "^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[a-z])" +         //at least 1 lower case letter
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=\\S+$)" +           //no white spaces
                    ".{8,}" +               //at least 8 characters
                    "$"
        )
        return passwordREGEX.matcher(password).matches()
    }
}