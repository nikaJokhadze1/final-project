package com.example.nikaverse.fragments

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.nikaverse.R
import com.example.nikaverse.UserInfo
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.regex.Pattern

class RegistrationFragment : Fragment(R.layout.fragment_registration) {
    private lateinit var layoutEmail: TextInputLayout
    private lateinit var layoutPass: TextInputLayout
    private lateinit var layoutConf: TextInputLayout
    private lateinit var layoutUser: TextInputLayout
    private lateinit var editTextUser: TextInputEditText
    private lateinit var editTextEmail: TextInputEditText
    private lateinit var editTextPassword: TextInputEditText
    private lateinit var editTextConfirmPassword: TextInputEditText
    private lateinit var buttonRegister: Button
    private lateinit var buttonBack: ImageView
    val db = FirebaseDatabase.getInstance().getReference("userinfo")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        registerListeners()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    val action =
                        RegistrationFragmentDirections.actionRegistrationFragmentToLoginFragment()
                    findNavController().navigate(action)
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            callback
        )
    }

    private fun init() {
        layoutEmail = view?.findViewById(R.id.layoutEmail)!!
        layoutConf = view?.findViewById(R.id.layoutConfirm)!!
        layoutPass = view?.findViewById(R.id.layoutPassword)!!
        layoutUser = view?.findViewById(R.id.layoutUser)!!
        editTextEmail = view?.findViewById(R.id.editTextEmail)!!
        editTextPassword = view?.findViewById(R.id.editTextPassword)!!
        editTextConfirmPassword = view?.findViewById(R.id.editTextConfirmPassword)!!
        buttonRegister = view?.findViewById(R.id.buttonRegister)!!
        editTextUser = view?.findViewById(R.id.editTextUser)!!
        buttonBack = view?.findViewById(R.id.backButton)!!
    }

    private fun registerListeners() {
        buttonRegister.setOnClickListener {
            val username = editTextUser.text.toString().lowercase()
            val email = editTextEmail.text.toString().lowercase()
            val password = editTextPassword.text.toString()
            val confirmPassword = editTextConfirmPassword.text.toString()
            if (username.isEmpty() || username.length < 4 || username.length > 10) {
                Toast.makeText(
                    activity,
                    "Your username must be between 4 to 10 letters ", Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            if (email.isEmpty() || password.isEmpty()) {
                Toast.makeText(activity, "Empty!!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (!emailChecker(email)) {
                Toast.makeText(activity, "E-mail is invalid", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.length < 8) {
                Toast.makeText(
                    activity,
                    "Password must be at least 8 characters long",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (!isValidPassword(password)) {
                Toast.makeText(
                    activity,
                    "Password must contain Letters and Numbers",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            if (password != confirmPassword) {
                Toast.makeText(activity, "Passwords must match each other", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            db.orderByChild("username").equalTo(username)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        if (!snapshot.exists()) {
                            FirebaseAuth.getInstance()
                                .createUserWithEmailAndPassword(email, password)
                                .addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        val uid = FirebaseAuth.getInstance().uid ?: ""
                                        db.child(uid)
                                            .setValue(
                                                UserInfo(
                                                    username.lowercase(),
                                                    email.lowercase(),
                                                    uid
                                                )
                                            )
                                        findNavController().navigate(R.id.loginFragment)
                                    } else {
                                        Toast.makeText(activity, "Errotkh!", Toast.LENGTH_SHORT)
                                            .show()
                                    }
                                }
                        } else {
                            Toast.makeText(activity, "Username Exists!", Toast.LENGTH_SHORT).show()
                        }

                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }
                })


        }

        buttonBack.setOnClickListener {
            val action = RegistrationFragmentDirections.actionRegistrationFragmentToLoginFragment()
            findNavController().navigate(action)
        }
    }

    private fun isValidPassword(password: String): Boolean {
        val passwordREGEX = Pattern.compile(
            "^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[a-z])" +         //at least 1 lower case letter
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=\\S+$)" +           //no white spaces
                    ".{8,}" +               //at least 8 characters
                    "$"
        )
        return passwordREGEX.matcher(password).matches()
    }

    private fun emailChecker(Email: String): Boolean {
        return !TextUtils.isEmpty(Email) && Patterns.EMAIL_ADDRESS.matcher(Email).matches()
    }
}